\documentclass[aspectratio=169,9pt]{beamer}

\include{header}
%\usebackgroundtemplate{\includegraphics[width=\paperwidth]{NormalANLBlue}}
\title[PETSc 2018]
{Understanding and Tuning Performance in PETSc \\
(on emerging manycore {\bf and} traditional architectures)}
\author[Speaker]{Richard Tran Mills \\
(with contributions from Karl Rupp, Matt Knepley, Jed Brown, Hong Zhang, 
and Hong Zhang)}
\subtitle{}
\institute[ANL/MCS]{PETSc User Meeting 2018, Imperial College London, London, UK}
\date{June 4, 2018}

\usepackage[normalem]{ulem}
\usepackage[]{xcolor}
\usepackage{color}
\usepackage{listings}

\definecolor{darkblue}{rgb}{0,0,.6}
\definecolor{darkred}{rgb}{.6,0,0}
\definecolor{darkgreen}{rgb}{0,.6,0}
\definecolor{red}{rgb}{.98,0,0}
\definecolor{lightgrey}{rgb}{0.98,0.98,0.98}

\lstloadlanguages{C++}
\lstset{%
  language=C++,
  basicstyle=\small\ttfamily,
  commentstyle=\itshape\color{darkgreen},
  keywordstyle=\bfseries\color{darkblue},
  stringstyle=\color{darkred},
  showspaces=false,
  showtabs=false,
  columns=fixed,
  backgroundcolor=\color{lightgrey},
  numbers=none,
  frame=single,
  numberstyle=\tiny,
  breaklines=true,
  showstringspaces=false,
  xleftmargin=0.1cm,
  xrightmargin=3em
}%

\lstset{emphstyle=\color{red}}

\newcommand{\magenta}{\textcolor{magenta}}

\begin{document}

\setbeamertemplate{footline}{}
{
\usebackgroundtemplate{\includegraphics[width=\paperwidth]{TitleANLBlue}}
\frame{\titlepage}
}

\setbeamertemplate{footline}[page number]{}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Table of Contents}

Hardware Architectural Trends: Power, FLOPS, and Bandwidth \\
~\\
Performance Tuning Strategies For These Trends\\
~\\
Performance Modeling \\
~\\
PETSc Profiling \\
~\\
Support for Manycore Architectures (with examples from ``Knights Landing'' Intel Xeon Phi)

\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{What is driving current HPC trends?}

\begin{block}{Moore's Law (1965)}
\begin{itemize}
\item Moore's Law: Transistor density doubles roughly every two years
\item (Slowing down, but reports of its death have been greatly exaggerated.)
\item For decades, single core performance roughly tracked Moore's law growth, 
      because smaller transitors can switch faster.
\end{itemize}
\end{block}

\begin{block}{Dennard Scaling (1974)}
\begin{itemize}
\item Dennard Scaling: Voltage and current are proportional to linear dimensions of a transistor; 
      therefore power is proportional to the area of the transistor.
\item Ignores leakage current and threshold voltage; past 65 nm feature size, Dennard scaling breaks 
      down and power density increases, because these don't scale with feature size.
\end{itemize}
\end{block}

\begin{block}{Power Considerations}
\begin{itemize}
\item The ``power wall'' has limited practical processor frequencies to around 4 GHz since 2006.
\item Increased parallelism (cores, hardware threads, SIMD lanes, GPU warps, etc.) is the current path forward.

\end{itemize}
\end{block}

\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Microprocessor Trend Data}
 \begin{center}
  \includegraphics[width=0.8 \textwidth]{figures/42-years-processor-trend.pdf}
 \end{center}
 {\tiny https://www.karlrupp.net/2018/02/42-years-of-microprocessor-trend-data/}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Current trends in HPC architectures}

\begin{block}{Emerging architectures are very complex...}
  \begin{itemize}
  \item Lots of hardware cores, hardware threads
  \item Wide SIMD registers
  \item Increasing reliance on fused-multiply-add (FMA), with multiple execution ports, 
        proposed quad FMA instructions
  \item Multiple memories to manage (multiple NUMA nodes, GPU vs. host, normal vs. high-bandwidth RAM, 
  byte-addressable NVRAM being introduced, ...)
  \item Growing depth of hierarchies: in memory subsystem, interconnect topology, I/O systems
  \end{itemize}
\end{block}

\begin{block}{...and hard to program} 
  \begin{itemize}
  \item Vectorization may require fighting the compiler, or entirely re-thinking algorithm. 
  \item Must balance vectorization with cache reuse.
  \item Host vs.\ offload adds complexity; large imbalance between memory bandwidth on device vs. between host and device
  \item Growth in peak FLOP rates have greatly outpaced available memory bandwidth.
  \end{itemize}
\end{block}

\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Table of Contents}

Hardware Architectural Trends: Power, FLOPS, and Bandwidth \\
~\\
{\bf \textcolor{blue}{Performance Tuning Strategies For These Trends}}\\
~\\
Performance Modeling \\
~\\
PETSc Profiling \\
~\\
Support for Manycore Architectures (with examples from ``Knights Landing'' Intel Xeon Phi)

\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\input{./slides/performance_tuning.tex}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Table of Contents}

Hardware Architectural Trends: Power, FLOPS, and Bandwidth \\
~\\
Performance Tuning Strategies For These Trends \\
~\\
{\bf \textcolor{blue}{Performance Modeling}} \\
~\\
PETSc Profiling \\
~\\
Support for Manycore Architectures (with examples from ``Knights Landing'' Intel Xeon Phi)

\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Importance of Performance Models and Measurements}

We're almost getting ahead of ourselves:
We've already discussed several strategies for dealing with the memory hierarchies 
and flop/byte balances trending with manycore CPUs.\\
~\\
Before performance tuning, we should consider two rules.\\
~\\

\begin{block}{Rule 1: Don't try to tune/optimize code performance without meaningful performance measurements.}
  \begin{itemize}
  \item Otherwise, you can waste tons of time ``optimizing'' things that will make no difference. 
  \end{itemize}
\end{block}

\begin{block}{Rule 2: Don't think you have meaningful performance measurements if you don't have a performance model.}
  \begin{itemize}
  \item Needed to help verify your implementation.
  \item Needed to have some understanding of why the performance is what it is, and how it can be improved. 
        Otherwise, you are doomed to trying random things and hoping they make the code faster.
  \item Even a very crude, approximate model is better than no model. 
  \end{itemize}
\end{block}

\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Karl's performance modeling slides
\input{slides/bottlenecks.tex}
\input{slides/modeling-examples.tex}

% Matt's SpMV model slide.
\input{slides/SMVAnalysis.tex}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Table of Contents}

Hardware Architectural Trends: Power, FLOPS, and Bandwidth \\
~\\
Performance Tuning Strategies For These Trends\\
~\\
Performance Modeling \\
~\\
{\bf \textcolor{blue}{PETSc Profiling}} \\
~\\
Support for Manycore Architectures (with examples from ``Knights Landing'' Intel Xeon Phi)

\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% My slide about debug vs. optimized builds
\input{slides/debug_vs_optimized.tex}

% Karl's profiling slides
\input{slides/Profiling.tex}
\input{slides/ReadingLogSummary.tex}
\input{slides/CommunicationCosts.tex}
\input{slides/profiling-stages.tex}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Table of Contents}

Hardware Architectural Trends: Power, FLOPS, and Bandwidth \\
~\\
Performance Tuning Strategies For These Trends\\
~\\
Performance Modeling \\
~\\
PETSc Profiling \\
~\\
{\bf \textcolor{blue}{Support for Manycore Architectures}} (with examples from ``Knights Landing'' Intel Xeon Phi)

\begin{itemize}
\item Brief overview of Knights Landing (KNL)
\item ``Out-of-box'' experiences with PETSc on KNL
\item New features in PETSc targeting manycore
  \begin{itemize}
  \item Sliced ELLPACK matrix class (MATSELL)
  \item Matrix class leveraging Intel MKL sparse BLAS (MATAIJMKL)
  \item MPI-3 shared memory implementations of VecScatter (presentation by Hong Zhang)
  \end{itemize}
\end{itemize}

\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Manycore features and KNL examples
\input{slides/manycore_features.tex}

\end{document}


